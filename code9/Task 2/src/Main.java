import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/formative9";
    static final String USER = "root";
    static final String PASS = "root";

    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    public static void main(String[] args){
        try{
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql = "SELECT * FROM user";
            rs = stmt.executeQuery(sql);

            while(rs.next()){
                System.out.println("ID          : " + rs.getInt("id"));
                System.out.println("Fullname    : " + rs.getString("name"));
                System.out.println("Surname     : " + rs.getString("surname"));
            }

            stmt.close();
            rs.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
