import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


public class Main {
    public final static int SOCKET_PORT = 14236;
    public final static String RECEIVE_PATH = "D:\\Formative\\Formative 9\\code9\\Task 3\\from-server\\from-client.txt";
    public final static String SEND_PATH = "D:\\Formative\\Formative 9\\code9\\Task 3\\from-server\\from-server.txt";
    public final static int FILE_SIZE = 6000000;

    public static void main(String[] args) throws IOException {
        int byteRead;
        int current;
        ServerSocket serverSocket = null;
        Socket socket = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;

        try {
            serverSocket = new ServerSocket(SOCKET_PORT);
            while (true) {
                System.out.println("Waiting");
                try {
                    socket = serverSocket.accept();
                    System.out.println("Koneksi terhubung : " + socket);

                    byte[] byteArray = new byte[FILE_SIZE];
                    InputStream is = socket.getInputStream();
                    fos = new FileOutputStream(RECEIVE_PATH);
                    bos = new BufferedOutputStream(fos);
                    byteRead = is.read(byteArray, 0, byteArray.length);
                    current = byteRead;

                    do {
                        byteRead = is.read(byteArray, current, (byteArray.length - current));
                        if (byteRead >= 0) current += byteRead;
                    } while (byteRead > -1);

                    bos.write(byteArray, 0, current);
                    bos.flush();
                    System.out.println("File " + RECEIVE_PATH + " downloaded " + "(" + current + "bytes read");


                } finally {
                    if(fos != null) fos.close();
                    if(bos != null) bos.close();
                    if(socket != null) socket.close();
                }
            }
        } finally {
            if(fos != null)fos.close();
            if(bos != null)bos.close();
            if(socket != null)socket.close();
        }
    }

    public static void sendFile() throws IOException {
        BufferedInputStream bis = null;
        ServerSocket serverSocket = null;
        FileInputStream fis = null;
        OutputStream os = null;
        Socket socket = null;
        try {
            serverSocket = new ServerSocket(14237);
            socket = serverSocket.accept();
            System.out.println("Server akan mengirimkan anda file...");

            File sendfile = new File(SEND_PATH);
            byte[] sendByte = new byte[(int) sendfile.length()];
            fis = new FileInputStream(sendfile);
            bis = new BufferedInputStream(fis);
            bis.read(sendByte, 0, sendByte.length);
            os = socket.getOutputStream();
            System.out.println("Sending file : " + SEND_PATH + "(" + sendByte.length + " bytes" + ")");
            os.write(sendByte, 0, sendByte.length);
            os.flush();

            System.out.println("File terkirim ke client");
        } finally {
            if (bis != null) bis.close();
            if (os != null) os.close();
            if (socket != null) socket.close();
        }

    }
}

