import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;


public class Main {
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://google.com");
        URLConnection urlConnection = url.openConnection();
        InputStream stream = urlConnection.getInputStream();

        int i;
        while((i=stream.read()) != -1){
            System.out.print((char) i);
        }
    }
}
