import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


public class Client3 extends Thread {
    public final static int SOCKET_PORT = 14236;
    public final static String RECEIVE_PATH3 = "D:\\Formative\\Formative 9\\code9\\Task 4\\from-server3\\from-client.txt";
    public final static int FILE_SIZE = 6000000;

    @Override
    public void run() {
        int byteRead;
        int current;
        ServerSocket serverSocket = null;
        Socket socket = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;


        try {
            serverSocket = new ServerSocket(SOCKET_PORT);
            while (true) {
                System.out.println("Waiting");
                try {
                    socket = serverSocket.accept();
                    System.out.println("Koneksi terhubung : " + socket);

                    byte[] byteArray = new byte[FILE_SIZE];
                    InputStream is = socket.getInputStream();
                    fos = new FileOutputStream(RECEIVE_PATH3);
                    bos = new BufferedOutputStream(fos);
                    byteRead = is.read(byteArray, 0, byteArray.length);
                    current = byteRead;

                    do {
                        byteRead = is.read(byteArray, current, (byteArray.length - current));
                        if (byteRead >= 0) current += byteRead;
                    } while (byteRead > -1);

                    bos.write(byteArray, 0, current);
                    bos.flush();
                    System.out.println("File " + RECEIVE_PATH3 + " downloaded " + "(" + current + "bytes read");


                } finally {
                    if(fos != null) fos.close();
                    if(bos != null) bos.close();
                    if(socket != null) socket.close();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

