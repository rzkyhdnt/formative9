import java.io.*;
import java.net.Socket;

public class Client {
    public final static int SOCKET_PORT = 14236;
    public final static String SERVER = "127.0.0.1";
    public final static String SEND_PATH = "D:\\Formative\\Formative 9\\code9\\Task 4\\from-client\\from-client.txt";
    public final static String RECEIVE_PATH = "D:\\Formative\\Formative 9\\code9\\Task 4\\from-client\\from-server.txt";
    public final static int FILE_SIZE = 60000000;

    public static void main(String[] args) throws IOException {
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        OutputStream os = null;
        Socket socket = null;

        try {
            try {
                socket = new Socket(SERVER, SOCKET_PORT);
                System.out.println("Connecting...");

                File myFile = new File(SEND_PATH);
                byte[] byteArray = new byte[(int) myFile.length()];
                fis = new FileInputStream(myFile);
                bis = new BufferedInputStream(fis);
                bis.read(byteArray, 0, byteArray.length);
                os = socket.getOutputStream();
                System.out.println("Sending file " + SEND_PATH + "(" + byteArray.length + " bytes" + ")");
                os.write(byteArray, 0, byteArray.length);
                os.flush();

                receiveFile();

                System.out.println("Done");
            } finally {
                if (bis != null) bis.close();
                if (os != null) os.close();
                if (socket != null) socket.close();
            }
        } catch (Exception e) {
        }
    }

    public static void receiveFile() throws IOException{
        int byteRead;
        int current;
        Socket socket = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;

        try {
            socket = new Socket(SERVER, 14237);

            while (true) {
                System.out.println("Waiting");
                try {
                    byte[] byteArray = new byte[FILE_SIZE];
                    InputStream is = socket.getInputStream();
                    fos = new FileOutputStream(RECEIVE_PATH);
                    bos = new BufferedOutputStream(fos);
                    byteRead = is.read(byteArray, 0, byteArray.length);
                    current = byteRead;

                    do {
                        byteRead = is.read(byteArray, current, (byteArray.length - current));
                        if (byteRead >= 0) current += byteRead;
                    } while (byteRead > -1);

                    bos.write(byteArray, 0, current);
                    bos.flush();
                    System.out.println("File " + RECEIVE_PATH + " downloaded " + "(" + current + "bytes read");

                } finally {
                    if(fos != null) fos.close();
                    if(bos != null) bos.close();
                    if(socket != null) socket.close();
                }
            }
        } finally {
            if(fos != null)fos.close();
            if(bos != null)bos.close();
            if(socket != null)socket.close();
        }
    }
}
